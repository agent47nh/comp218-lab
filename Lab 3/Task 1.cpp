#include<iostream>
#include<math.h>
using namespace std;
template <typename X>
X distance(X x[],X y[]){
	return sqrt(pow((y[1]-y[0]),2)+pow((x[1]-x[0]),2));
}
int main(void){
	cout<<"Enter the four values of the discrete points\nEnter X1:";
	int x[2],y[2];
	cin>>x[0];
	cout<<"Enter X2:";
	cin>>x[1];
	cout<<"\nEnter Y1:";
	cin>>y[0];
	cout<<"Enter Y2:";
	cin>>y[1];
	cout<<"The distance between the two points are "<< distance(x,y)<<endl;
	cout<<"Enter the four double values of the discrete points\nEnter X1:";
	double x1[2],y1[2];
	cin>>x1[0];
	cout<<"Enter X2:";
	cin>>x1[1];
	cout<<"\nEnter Y1:";
	cin>>y1[0];
	cout<<"Enter Y2:";
	cin>>y1[1];
	cout<<"The distance between the two points are "<< distance(x1,y1)<<endl;
	return 0;
}