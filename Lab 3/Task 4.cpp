#include<iostream>
#include<math.h>
#define PI 3.14
using namespace std;
inline double aoc(double rad){
	return PI*pow(rad,2);        //3.14^2
}
inline double vocyl(double rad, double height){
	return aoc(rad)*height;     //3.14^2*h
}
int main(void){
	cout<<"Enter the parameters of a circle(in centimeters):\nRadius: ";
	double rad,height;
	cin>>rad;
	cout<<"Height: ";
	cin>>height;
	cout<<"The Volume of cylinder: "<<vocyl(rad,height)<< "cm3"<<endl;
	return 0;
}