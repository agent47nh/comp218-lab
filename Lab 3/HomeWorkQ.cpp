/* 1. Considering that the size of char is 1 byte and size of int is 4 bytes, and there is no alignment done by the compiler. Please analyze the following code and write the output and your observation. */
#include<iostream>
#include<stdlib.h>
using namespace std;
 
template<class T, class U>
class A  {
    T x;
    U y;
    static int count;
};
 
int main()  {
   A<char, char> a;
   A<int, int> b;
   cout << sizeof(a) << endl;
   cout << sizeof(b) << endl;
   return 0;
}
/* 2. Please analyze this code and write your observation. */
#include<iostream.h>
#include<conio.h>
#include<stdlib.h>

int add(int, int);         // function prototype
int subtract(int, int);    // function prototype
int multiply(int, int);    // function prototype
int divide(int, int);      // function prototype

void main()
{
    clrscr();
    int a, b;
    cout<<"Enter any two number: ";
    cin>>a>>b;
    cout<<"\nSummation = "<<add(a, b);
    cout<<"\nSubtraction = "<<subtract(a, b);  
    cout<<"\nMultiplication = "<<multiply(a, b); 
    cout<<"\nDivision = "<<divide(a, b); 
    getch();
}

int add(int x, int y)     // function definition
{
    int res;
    res = x + y;
    return res;
}

int subtract(int x, int y)    // function definition
{
    int res;
    res = x - y;
    return res;
}

int multiply(int x, int y)    // function definition
{
    int res;
    res = x * y;
    return res;
}

int divide(int x, int y)    // function definition
{
    if(y==0)
    {
        cout<<"\n\nDivide by Zero Error..!!";
        cout<<"\nPress any key to exit...";
        getch();
        exit(1);
    }
    else
    {
        int res;
        res = x / y;
        return res;
    }
}
