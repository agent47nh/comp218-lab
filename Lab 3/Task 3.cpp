#include<iostream>
using namespace std;
int swap(int*k,int *y){
	int temp=*k;
	*k=*y;
	*y=temp;
}
int main(void){
	cout<<"Enter any two numbers\nFirst one: ";
	int l,a;
	cin>>l;
	cout<<"Another one: ";
	cin>>a;
	cout<<"l= "<<l<<" a= "<<a<<endl;
	swap(&l,&a);
	cout<<"Swapped\nl= "<<l<<", a= "<<a<<endl;
	return 0;
}