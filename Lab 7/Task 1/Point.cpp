#include "Point.h"
#include <iostream>
using namespace std;
Point::Point():x(0),y(0){}
Point::Point(int tx,int ty):x(tx),y(ty){}
Point::~Point(){}
Point::Point(const Point& other){
    x=other.x;
    y=other.y;
}
void Point::set_X(int tx){x=tx;}
void Point::set_Y(int ty){y=ty;}
int Point::get_X()const{return x;}
int Point::get_Y()const{return y;}
double Point::print()const{cout<<"["<<x<<","<<y<<"]"<<endl;}
