#ifndef LINE_H
#define LINE_H
#include "Point.h"
class line
{
    public:
        line();                                 //DONE
        line(int=1, int=1,int=1,int=1);         //DONE
        ~line();                                //DONE
        line(const line& other);    //DONE
        void set_point1(int,int);   //DONE
        void set_point2(int,int);   //DONE
        double get_slope() const;   //DONE
        void print() const;         //DONE
    private:
        Point p1,p2;
};

#endif
