#ifndef POINT_H
#define POINT_H

class Point{
    public:
        Point();                            //DONE
        Point(int,int);                     //DONE
        ~Point();                           //DONE
        Point(const Point& other);          //DONE
        void set_X(int);                    //DONE
        void set_Y(int);                    //DONE
        int get_X() const;                  //DONE
        int get_Y() const;                  //DONE
        double print() const;               //DONE
    private:
        int x,y;
};

#endif // POINT_H
