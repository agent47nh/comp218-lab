#include "line.h"
#include "Point.h"
#include<iostream>
#include<iomanip>
using namespace std;
line::line(){
    p1.set_X(0);
    p1.set_Y(0);
    p2.set_X(0);
    p2.set_Y(0);
}
line::~line(){}
line::line(int tx, int ty, int tx1, int ty1){
    p1.set_X(tx);
    p1.set_Y(ty);
    p2.set_X(tx1);
    p2.set_Y(ty1);
}
line::line(const line& other){
    p1=other.p1;
    p2=other.p2;
}
void line::set_point1(int tx,int ty){
    p1.set_X(tx);
    p1.set_Y(ty);
}
void line::set_point2(int tx,int ty){
    p2.set_X(tx);
    p2.set_Y(ty);
}
double line::get_slope() const{
    double x1=p1.get_X(),x2=p2.get_X(),y1=p1.get_Y(),y2=p2.get_Y();
    return (y2-y1)/(x2-x1);

}
void line::print() const{
    cout<<fixed;
    cout<<"A line passing thru "<<"["<<p1.get_X()<<", "<<p1.get_Y()<<"]"<<" and "<<"["<<p2.get_X()<<", "<<p2.get_Y()<<"]"<<" with slope = "<<setprecision(3)<<get_slope()<<endl;
}
