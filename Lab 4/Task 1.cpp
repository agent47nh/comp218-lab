#include<iostream>
#include<string>
#include<algorithm>			//Don't reinvent the Wheel!
using namespace std;
bool areyousure(){
	char confirm;
	cout<<"Are you sure? (y/n)"<<endl;
	cin>>confirm;
	if(confirm=='y'||confirm=='Y') return 1;
	else return 0;
}
class strangs{
private:
	int pos,pos2;
	string str,str1,ins;
public:
	void create();
	void insert();
	void erase();
	void append();
	void reversee();
	void print();
};
void strangs::create(){
	cout << "Enter any phrase: ";
	cin.ignore();			//To nullify the after-effects of cin...try commenting this line to see difference.
	getline(cin,str);
	cout<<"'"<<str<<"'"<<" has been recorded."<<endl;
}
void strangs::insert(){
	cout<<"Which position do you want to insert the character in?"<<endl;
	cin>> pos;
	cout<<"What do you want to enter?"<<endl;
	cin.ignore();			//To nullify the after-effects of cin...try commenting this line to see difference.
	getline(cin,ins);
	str.insert(pos-1,ins);
	cout<<"'"<<str<<"'"<<" is the updated phrase."<<endl;
}
void strangs::erase(){
	cout<<"'"<<str<<"'"<<" is the phrase"<<endl;
	cout<<"From which position do you want to erase?"<<endl;
	cin>>pos;
	cout<<"How many characters do you want to delete?"<<endl;
	cin>>pos2;
	if(areyousure()){
		str.erase(pos-1,pos2);
		cout<<"The updated phrase is "<<"'"<<str<<"'"<<"."<<endl;
	}
	else cout<<"No changes made."<<endl<<"'"<<str<<"'"<<" is the phrase."<<endl;
}
void strangs::append(){
	cout<<"What is the phrase that you want to append?"<<endl;
	cin.ignore();			//To nullify the after-effects of cin...try commenting this line to see difference.
	getline(cin,str1);
	if(areyousure()){
		str.append(str1);
		cout<<"The updated phrase is "<<"'"<<str<<"'"<<"."<<endl;
	}
	else cout<<"No changes made."<<endl<<"'"<<str<<"'"<<" is the phrase."<<endl;
}
void strangs::reversee(){
	cout<<"'"<<str<<"'"<<" is the phrase"<<endl;
	if(areyousure()){
		reverse(str.begin(), str.end());		//Don't reinvent the Wheel!
		cout<<"The updated phrase is "<<"'"<<str<<"'"<<"."<<endl;
	}
	else cout<<"No changes made."<<endl<<"'"<<str<<"'"<<" is the phrase."<<endl;
}
void strangs::print(){cout<<"The recorded phrase:\n"<<str<<endl;}
int main(){
	int select=9;
	strangs strangs1;
	while (select != 0){
		cout << "Select any option below : \n 1.Create\n 2.Insert\n 3.Erase\n 4.Append\n 5.Reverse\n 6.Print\n 0.Exit\nYour selection ===> ";
		cin >> select;
		switch (select){
		case 1:
			strangs1.create();
			break;
		case 2:
			strangs1.insert();
			break;
		case 3:
			strangs1.erase();
			break;
		case 4:
			strangs1.append();
			break;
		case 5:
			strangs1.reversee();
			break;
		case 6:
			strangs1.print();
			break;
		case 0:
			cout << "Bye! Have a Good day!"<<endl;
			break;
		default:
			cout << "Invalid Option! Try again?"<<endl;
		}
	}
}