# European University of Lefke
Course Code: COMP218    
Lecturer: **Asst. Prof. Dr. Ferhun Yorgancıoğlu**    
*For students taking course COMP218*  
---
**PLEASE USE THIS REPOSITORY FOR REFERENCE PURPOSES ONLY**   
**ALWAYS TRY CODING ON YOUR OWN**   

---
### Overview
COMP218 is an Object Oriented Programming (OOP) course, the language used in this course is C++. The objective of this course is to teach students paradigm (encapsulation, inheritance, and polymorphism) for developing solutions to well-defined computing problems using the C++programming language.    

---
### Aim & Objective
This repository contains Lab works and Homeworks for throughout the *Spring Semester of 2017-2018*.     
If there are any errors or you have a better and efficient method of coding please make pull request DON'T HESITATE!     
If you need any help or doubts please contact me thru my email only!    
E-mail: nhone4@gmail.com     

---
