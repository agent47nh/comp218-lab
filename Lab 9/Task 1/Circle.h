//
// Created by AS108-104 on 14.05.2018.
//

#ifndef UNTITLED_CIRCLE_H
#define UNTITLED_CIRCLE_H


#include "Point.h"

class Circle: public Point {
private:
    int r;
    Point p1,p2;
public:
    Circle ();
    void set_r(double);
    double get_r();
    void print();

};


#endif //UNTITLED_CIRCLE_H
