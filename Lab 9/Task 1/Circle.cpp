//
// Created by AS108-104 on 14.05.2018.
//

#include <iostream>
#include <cmath>
#include <iomanip>
#include "Circle.h"
Circle::Circle() {
    p1.set_x(10);
    p1.set_y(10);
    p2.set_x(50);
    p2.set_y(50);
}
void Circle::set_r(double tr) {r=tr;}
double Circle::get_r() {return r;}
void Circle::print() {
    double dist1=p2.get_x()-p1.get_x(),dist2=p2.get_y()-p1.get_y();
    double distance=sqrt(dist2+dist1);
    double radius=distance/2;
    double circumference=2*3.14*radius;
    double area=pow(3.14,2);
    std::cout<<"The radius of the circle is "<<radius<<std::endl;
    std::cout<<std::fixed;
    std::cout<<"The circumference is "<<std::setprecision(3)<<circumference<<std::endl<<std::setprecision(3)<<"The area is "<<area<<std::endl;
}