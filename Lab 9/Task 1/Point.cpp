//
// Created by AS108-104 on 14.05.2018.
//

#include <iostream>
#include "Point.h"
Point::Point()= default;
void Point::set_x(int tx) {x=tx;}
void Point::set_y(int ty) {y=ty;}
int Point::get_x() {return x;}
int Point::get_y() {return y;}
void Point::print(){std::cout<<"The points are ["<<get_x()<<", "<<get_y()<<"]."<<std::endl;}