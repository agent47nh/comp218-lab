//
// Created by AS108-104 on 14.05.2018.
//

#ifndef UNTITLED_POINT_H
#define UNTITLED_POINT_H


class Point {
protected:
    int x,y;
public:
    Point();
    void set_x(int);
    int get_x();
    void set_y(int);
    int get_y();
    void print();
};


#endif //UNTITLED_POINT_H
