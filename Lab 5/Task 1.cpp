#include<iostream>
#include<iomanip>
#include<cmath>
using namespace std;
class rectangle{
private:
	double length=1,breadth=1,diagonal;
public:
	rectangle();			//done
	rectangle(double,double);	//done
	void setLength(double);		//done
	void setBreadth(double);	//done	
	void getDiagonal();		//done
	void getArea();			//done
	void getCircumference();	//done
	void calcDiagonal();		//done
	~rectangle();
};
rectangle::rectangle(){
	cout<<"Constructor Called! Default Parameters are set\nLength=20\nBreadth=5"<<endl;
	length=20;breadth=5;
}
rectangle::rectangle(double lenghto,double breadtho){
	cout<<"Constructor Called!"<<endl;
	length=lenghto;breadth=breadtho;
}
void rectangle::setLength(double lengtho){
	length=lengtho;
	if(length<=0) {length=20;cout<<"Enter a correct value!\nValue defaulted to 20"<<endl;}
}
void rectangle::setBreadth(double breadtho){
	breadth=breadtho;
	if(breadth<=0) {breadth=5;cout<<"Enter a correct value!\nValue defaulted to 5"<<endl;}
}
void rectangle::calcDiagonal(){
	diagonal=sqrt(pow(length,2)+pow(breadth,2));
}
void rectangle::getDiagonal(){
	cout<<"The diagonal of the rectangle is "<<diagonal<<"."<<endl;
}
void rectangle::getArea(){
	cout<<fixed;
	cout<<"The area of the rectangle is "<<setprecision(3)<<length*breadth<<"."<<endl;
}
void rectangle::getCircumference(){
	cout<<fixed;
	cout<<"The circumference of the rectangle is "<<setprecision(3)<<2*(length+breadth)<<"."<<endl;
}
rectangle::~rectangle(){
	cout<<"Destructor Called!"<<endl;
}
int main(){
	rectangle obj1(5,7);
	double length,breadth;
	obj1.calcDiagonal();
	obj1.getArea();
	obj1.getDiagonal();
	obj1.getCircumference();
	cout<<"::Enter the length of the Rectangle::"<<endl;
	cin>>length;
	obj1.setLength(length);
	cout<<"::Enter the breadth of the Rectangle::"<<endl;
	cin>>breadth;
	obj1.setBreadth(breadth);
	obj1.calcDiagonal();
	obj1.getArea();
	obj1.getDiagonal();
	obj1.getCircumference();
}