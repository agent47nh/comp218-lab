#ifndef L5T2_H
#define L5T2_H

class rectangle{
private:
	double length=1,breadth=1,diagonal;
public:
	rectangle();				//done
	rectangle(double,double);	//done
	void setLength(double);		//done
	void setBreadth(double);	//done	
	void getDiagonal();			//done
	void getArea();				//done
	void getCircumference();	//done
	void calcDiagonal();		//done
	~rectangle();
};

#endif