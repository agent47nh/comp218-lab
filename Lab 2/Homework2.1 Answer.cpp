#include<iostream>
#define PI 3.14
using namespace std;
void is_prime(int prime){
	int flag=0;
	for(int i=2; i<=prime/2; ++i){
		if(prime%i==0){
			flag=1;
			break;
		}
	}
	if(prime<=1)
		flag=1;
	if(flag)
		cout<<"\n"<<prime<<" is not a Prime Number\n"<<endl;
	else
		cout<<"\n"<<prime<<" is a Prime Number\n"<<endl;
}
void circleAnC(float rad){
	float circum, area;
	circum=2*PI*rad;				//Circle Circumference 2(PI)r
	area=PI*rad*rad;				//Circle Area (PI)r^2
	cout<<"\nArea is "<<area<<" and circumference is "<<circum<<"\n"<<endl;
}
float triangleP(float P[]){
	return P[0]+P[1]+P[2];				//Perimeter of Triangle a+b+c
}
float triangleA(float A[]){
	return ((A[0]*A[1])/2);				//Area of Triangle (bh)/2
}
void trianglePnA(){
	char choice;float peri[3],area[2];
	cout<<"For finding,\nPerimeter of Triangle, Enter 'a'\nArea of Triangle, Enter 'b'\nFor both, Enter 'c'\n";
	cin>>choice;
	switch(choice){
	case 'a':
		for(int temp=0;temp<3;temp++){
			cout<<"Enter the length of side "<<temp+1<<": ";
			cin>>peri[temp];
		}
		cout<<"The perimeter of the triangle is "<<triangleP(peri)<<endl;
		break;
	case 'b':
		cout<<"Enter the height of triangle: ";
		cin>>area[0];
		cout<<"Enter the breadth of triangle: ";
		cin>>area[1];
		cout<<"The area of the triangle is "<<triangleA(area)<<endl;
		break;
	case 'c':
		cout<<"Enter the height of triangle: ";
		cin>>area[0];
		cout<<"Enter the breadth of triangle: ";
		cin>>area[1];
		for(int temp=0;temp<3;temp++){
			cout<<"Enter the length of side "<<temp+1<<": ";
			cin>>peri[temp];
		}
		cout<<"\n\nThe perimeter of the triangle is "<<triangleP(peri)<<endl;
		cout<<"The area of the triangle is "<<triangleA(area)<<endl<<endl;		
	}
}
float cubeSA(float side){
	return 6*side*side;				//Surface area of cube 6a^2
}
float cubeVL(float side){
	return side*side*side;				//Volume of cube a^3
}
float parallelgmA(float pl[]){
	return pl[0]*pl[1];				//Area of Parallelogram b*h
}							//pl[0]=breadth,pl[1]=height
float parallelgmP(float pl[]){
	return 2*(pl[0]+pl[1]);				//Perimeter of Parallelogram 2(b+h)
}							//pl[0]=side1,pl[1]=side2
void parallelgmPnA(){
	char choice;float side[2];
	cout<<"Find:\na. Find perimeter of parallelogram.\nb. Find area of parallelogram\nc. Find both"<<endl;
	cin>>choice;
	switch(choice){
	case 'a':
		cout<<"Enter the value of side 1: ";
		cin>>side[0];
		cout<<"Enter the value of side 2: ";
		cin>>side[1];
		cout<<"The perimeter of parallelogram is "<<parallelgmP(side)<<endl;
		break;
	case 'b':
		cout<<"Enter the value of side 1: ";
		cin>>side[0];
		cout<<"Enter the value of side 2: ";
		cin>>side[1];
		cout<<"The area of parallelogram is "<<parallelgmA(side)<<endl;
		break;
	case 'c':
		cout<<"Enter the value of side 1: ";
		cin>>side[0];
		cout<<"Enter the value of side 2: ";
		cin>>side[1];
		cout<<"\n\nThe area of parallelogram is "<<parallelgmA(side)<<endl<<"The perimeter of parallelogram is "<<parallelgmP(side)<<"\n"<<endl;
		break;
	}
}
float cylinderVl(float ar[]){
	return 2*PI*ar[0]*ar[0]*ar[1];			//Volume of Cylinder 2(PI)r^2h
}							//ar[0]=radius,ar[1]=height
void cylinder(){
	float ar[2];
	cout<<"Enter the radius of cylinder: ";
	cin>>ar[0];
	cout<<"Enter the height of the cylinder: ";
	cin>>ar[1];
	cout<<"\nThe volume of cylinder is "<<cylinderVl(ar)<<"\n"<<endl;
}
float sphereVl(float rad){
	return ((4/3)*PI*rad*rad*rad);			//Volume of Sphere (4/3)*(PI)*r^3
}
void sphere(){
	float rad;
	cout<<"Enter the radius of the Sphere: ";
	cin>>rad;
	cout<<"\nThe volume of the Sphere is "<<sphereVl(rad)<<"\n"<<endl;
}
int main(void){
	int choice;char choice1,choice2;
	while(choice!=4){
		cout<<"Choose any option from the menu below:\n1. Prime Number\n2. Calculate area and circumference of:\n3. Volume of:\n4. Exit"<<endl;
		cin>>choice;
		switch(choice){
		case 1:
			int prime;
			cout<<"Enter any Prime number: ";
			cin>>prime;
			is_prime(prime);
			break;
		case 2:
			while(choice1!=101){
				cout<<"2. Calculate area and circumference of:"<<endl;
				cout<<"\na. Circle\nb. Triangle\nc. Cube\nd. Parallelogram\ne. Back to the Main Menu"<<endl;
				cin>>choice1;
				switch(choice1){
				case 'a':
					float rada;
					cout<<"Enter the radius of circle: ";
					cin>>rada;
					circleAnC(rada);
					break;
				case'b':
					trianglePnA();
					break;
				case 'c':
					float side;
					cout<<"Enter value of any side of the cube: ";
					cin>>side;
					cout<<"\nThe surface area of the cube is "<<cubeSA(side)<<"\n"<<endl;
					break;
				case 'd':
					parallelgmPnA();
					break;
				case 'e':
					cout<<"Back to main menu..."<<endl;
					break;
				default:
					cout<<"Invalid Input! Try again..."<<endl;		
				}
			}
			break;
		case 3:
			while(choice2!=105){
				cout<<"3. Volume of:"<<endl;
				cout<<"\nf. Cylinder\ng. Sphere\nh. Cube\ni. Back to Main Menu\n"<<endl;
				cin>>choice2;
				switch(choice2){
				case 'f':
					cylinder();
					break;
				case 'g':
					sphere();
					break;
				case 'h':
					float side;
					cout<<"Enter value of any side of the cube: ";
					cin>>side;
					cout<<"\nThe volume of the cube is "<<cubeVL(side)<<"\n"<<endl;
					break;
				case 'i':
					cout<<"Back to main menu..."<<endl;
					break;
				default:
					cout<<"Invalid Input! Try again...";	
				}
			}
			break;
		case 4:
			cout<<"Exiting..."<<endl;
			break;
		default:
			cout<<"Invalid Input! Try again..."<<endl;
		}
	}
}