### Write a menu-driven program in C++ which provides the following options:
1.       Prime Number: 
2.       Calculate the area and circumference of:             
            a.       Circle   
            b.       Triangle   
            c.       Cube   
            d.       Parallelogram   
            e.       Back to the main menu   
3.       Volume of:   
            f.       Cylinder   
            g.       Sphere   
            h.       Cube   
            k.       Back to the main menu   
4.       Exit

## Note: All the variables MUST BE ENTERED BY THE USER.
#### Write the code, the output, and then try to write your observation.
