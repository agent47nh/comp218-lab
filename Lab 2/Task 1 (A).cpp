#include<iostream>
#include<iomanip>
using namespace std;
int main(void){
	cout << "Enter 5 floating point values:" << endl;
	float value[5], total = 0.0f;
	for (int i = 0; i <= 4; i++){
		cout << "Enter value " << i+1 << ": ";
		cin >> value[i];
		total += value[i];
	}
	cout << "Entry successful!" << endl;		
	cout << "The total is " <<setprecision(7)<< total<<endl;
	system("PAUSE");
	return 0;
}