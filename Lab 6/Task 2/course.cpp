#include <iostream>
#include <string>
#include "course.h"
using namespace std;
course::course(string tTitle,string tCode, int tCredit):title(tTitle), code(tCode), credit(tCredit){}
course::~course(){}
void course::setTitle(string tTitle){title=tTitle;}
void course::setCode(string tCode){code=tCode;}
void course::setCredit(int tCredit){
	credit=tCredit>2&&tCredit<=4?tCredit:3;
}
string course::getTitle() const{return title;}
string course::getCode() const{return code;}
int course::getCredit() const{return credit;}
string course::printCourse() const{return code+" - "+title;}