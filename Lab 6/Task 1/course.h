#ifndef COURSE_H
#define COURSE_H
#include<string>
using namespace std;
class course{
private:
	int credit;string title,code;
public:
	course(string="",string="",int=3);			//DONE
	~course();									//DONE
	void setTitle(string tTitle);				//DONE
	void setCode(string tCode);					//DONE
	void setCredit(int tCredit);				//DONE
	string getTitle() const;					//DONE
	string getCode() const;						//DONE
	int getCredit() const;						//DONE
	string printCourse() const;					//DONE
};

#endif