#include <iostream>
#include <string>
#include "course.h"
using namespace std;
int main(){
	int size,tcrdt;string tname;char wish;
	cout<<"How many courses do you want to save?"<<endl;
	cin>>size;
	course obj[size];
	for (int i=0;i<size;i++){
		cout<<"::Enter title of the Course ["<<i+1<<"]::"<<endl;
		cin.ignore();
		getline(cin,tname);
		obj[i].setTitle(tname);
		cout<<"::Enter code of "<<obj[i].getTitle()<<" course::"<<endl;
		getline(cin,tname);
		obj[i].setCode(tname);
		cout<<"::Enter credit of "<<obj[i].getTitle()<<" course::"<<endl;
		cin>>tcrdt;
		obj[i].setCredit(tcrdt);
	}
	cout<<"Do you want to list the saved courses? (Y/N): ";
	cin>>wish;
	if(wish='Y'||'y'){
		cout<<"List of saved courses are:"<<endl;
		for(int i=0;i<size;i++)
			cout<<i+1<<". "<<obj[i].printCourse()<<endl;
	}
}